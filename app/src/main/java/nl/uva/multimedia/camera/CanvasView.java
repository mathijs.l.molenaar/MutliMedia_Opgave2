package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CanvasView extends View {
    private Bitmap m_image = null;

    public CanvasView(Context context) {
        super(context);
    }

    public CanvasView(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    public CanvasView(Context context, AttributeSet attributes, int style) {
        super(context, attributes, style);
    }

    /* Called whenever the canvas is dirty and needs redrawing. */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

		/* Paint a image if we have it, just a demo, clean this up so it works
		 * your way, or remove it if you don't need it
		 */
        if (m_image != null) {
            Rect rect = new Rect(
                    getWidth() / 2 - m_image.getWidth() / 2,
                    getHeight() / 2 - m_image.getHeight() / 2,
                    getWidth() / 2 + m_image.getWidth() / 2,
                    getHeight() / 2 + m_image.getHeight() / 2
            );
            Paint bmp = new Paint();

            Log.e("CanvasView", "width: " + m_image.getWidth() + "; Height: " + m_image.getHeight());
            canvas.drawBitmap(m_image, null, rect, bmp);
        }
    }

    /* Accessors */
    public Bitmap getSelectedImage() {
        return m_image;
    }

    public void setSelectedImage(Bitmap image) { m_image = image; }
}

