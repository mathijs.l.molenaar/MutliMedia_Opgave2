package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;

class RotationSlider extends SeekBar implements SeekBar.OnSeekBarChangeListener {
    private CameraCapture m_camera_capture = null;

    /* Necessary constructors */
    public RotationSlider(Context context) {
        super(context);
        setup();
    }

    public RotationSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public RotationSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }

    private void setup() {
        setOnSeekBarChangeListener(this);
    }

    /* This method is called when the user moves the slider. */
    public void onProgressChanged(SeekBar slider, int progress, boolean from_user) {
        /* Do something with progress (the new position of the slider) here... */
        m_camera_capture.setRotationAngle(progress);
    }

    /* Part of interface, but unused. */
    public void onStartTrackingTouch(SeekBar slider) {
    }

    public void onStopTrackingTouch(SeekBar slider) {
    }

    public void setCameraCapture(CameraCapture cameraCapture) { m_camera_capture = cameraCapture; }
}
