package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

/* 
 * This file contains the main activity and all plumbing setup between
 * the different instances
 */

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/* Main activity */
public class CameraActivity extends Activity implements ActionBar.OnNavigationListener {
    /* Change this to enable the image chooser */
    final private boolean SHOW_SELECT_IMAGE = false;
    final private int SELECT_IMAGE_CODE = 42;

    private CameraCapture m_camera_capture = new CameraCapture();
    private CanvasView m_canvas_view = null;
    private RotationSlider m_slider = null;
    private CameraView m_camera_view = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Setup the actionbar */
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        ArrayList<String> itemList = new ArrayList<String>();
        itemList.add(getString(R.string.nearest_neighbour));
        itemList.add(getString(R.string.bilineair));
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(actionBar.getThemedContext(), android.R.layout.simple_list_item_1,
                android.R.id.text1, itemList);
        actionBar.setListNavigationCallbacks(spinnerAdapter, this);
        actionBar.setSelectedNavigationItem(1);

        setContentView(R.layout.main);
		
		/* Grab the views and widgets from the lay-out. */
        m_camera_view = (CameraView) findViewById(R.id.cameraView);
        m_canvas_view = (CanvasView) findViewById(R.id.canvasView);
        m_slider = (RotationSlider) findViewById(R.id.slider);
		
		/* Do some basic plumbing */
        m_camera_capture.setCanvasView(m_canvas_view);
        m_slider.setCameraCapture(m_camera_capture);
        m_slider.setMax(360);
        m_slider.setProgress(45);

		/* Can also be BEST_FIT, but BEST might be larger then
		 * the actual size of the preview canvas, experiment
		 * with this. Even with LARGEST_FIT it can fallback on
		 * BEST_FIT if no image is small enough.
		 */
        m_camera_view.setSizeType(CameraView.SizeType.LARGEST_FIT);
        m_camera_view.setPreviewCallback(m_camera_capture);
    }

    @Override
    protected void onResume() {
        super.onResume();

        m_camera_view.acquireCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();

        AsyncTask task = m_camera_capture.getTask();
        if (task != null)
            task.cancel(true);

        m_camera_view.releaseCamera();
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
	
		/* Need to reacquire camera on these */
        m_camera_view.releaseCamera();
        m_camera_view.acquireCamera();
    }

    /* Menu options handling, you probably don't need to change this */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.option_menu, menu);

        MenuItem camera_switch = menu.findItem(R.id.camera_switch),
                select_image = menu.findItem(R.id.select_image);
        
        /* Only enable the switch button if there is something to switch between. */
        camera_switch.setEnabled(Camera.getNumberOfCameras() > 1);

        select_image.setVisible(SHOW_SELECT_IMAGE);

        return true;
    }

    /* This method specifies the functionality for the menu items. */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.camera_switch:
                if (m_camera_view != null) {
                    m_camera_view.nextCamera();
                }
                return true;
            case R.id.select_image:
                intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(
                        Intent.createChooser(intent, "Select Image"),
                        SELECT_IMAGE_CODE
                );
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* This method is called when the image chooser activity has a result
     * and returns to this activity.
     */
    @Override
    protected void onActivityResult(int request_code, int result_code, Intent data) {
        super.onActivityResult(request_code, result_code, data);

        if (request_code == SELECT_IMAGE_CODE && result_code == RESULT_OK && data != null) {
            Uri selected_image;
            Cursor cursor;
            String path;

            selected_image = data.getData();
            cursor = getContentResolver().query(selected_image,
                    new String[]{MediaStore.Images.Media.DATA}, null, null, null);

            cursor.moveToFirst();

            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            m_canvas_view.setSelectedImage(BitmapFactory.decodeFile(path));
        }
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        switch (itemPosition) {
            case 0:
                m_camera_capture.setInterpolation(CameraCapture.Interpolation.NEAREST_NEIGHBOUR);
                break;
            case 1:
                m_camera_capture.setInterpolation(CameraCapture.Interpolation.BILINEAIR);
                break;
        }
        return true;
    }
}

