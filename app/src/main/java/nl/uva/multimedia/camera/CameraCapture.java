package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */


/* Handles the camera data 
 *
 * The size is based on the size of the onscreen preview and calculated in
 * CameraView. This means that smaller screen devices, will, yes, have less
 * to calculate on.
 */

import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import static java.lang.Math.round;

class CameraCapture implements CameraView.PreviewCallback {
    private CanvasView m_canvas_view = null;
    private int m_image_width = 0;
    private int m_image_height = 0;
    private int m_rotation_angle = 0;
    private Interpolation m_interpolation = null;
    private AsyncTask m_task = null;

    public enum Interpolation {
        NEAREST_NEIGHBOUR,
        BILINEAIR
    };

    /* Is called by Android when a frame is ready */
    public void onPreviewFrame(byte[] data, Camera camera, boolean rotated) {
        if (m_task == null || m_task.getStatus() == AsyncTask.Status.FINISHED) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

		    /* Rotated is true if the height and width parameters should be swapped
             * due to the image having been rotated internally. (Turns out it is
		     * rather tricky to patch a method in Java....)
		     */
            if (rotated) {
                int holder;
                holder = size.height;
                size.height = size.width;
                size.width = holder;
            }

            m_image_width = size.width;
            m_image_height = size.height;

            m_task = new CalculateImageTask().execute(new byte[][]{data});
        }
    }


    private class CalculateImageTask extends AsyncTask<byte[], Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(byte[]... data) {
            int[] origArgb = new int[m_image_width * m_image_height];
            int[] argb = new int[m_image_width * m_image_height];

		    /* Use the appropriate YUV conversion routine to retrieve the
		    * data we actually intend to process.
		    */
            CameraData.convertYUV420SPtoARGB(origArgb, data[0], m_image_width, m_image_height);

		    /* Work on the argb array... */
            double sinA = Math.sin(Math.toRadians(m_rotation_angle));
            double cosA = Math.cos(Math.toRadians(m_rotation_angle));
            int mX = m_image_width / 2;
            int mY = m_image_height / 2;

            if (m_rotation_angle == 0 || m_rotation_angle == 360) {
                argb = origArgb;
            } else if (m_interpolation == Interpolation.NEAREST_NEIGHBOUR) {
                /* Nearest Neighbour Interpolation */
                for (int y = 0; y < m_image_height; y++) {
                    int offset = y * m_image_width;
                    /* y as having ZERO in the centre of the image */
                    int sY = y - mY;
                    for (int x = 0; x < m_image_width; x++) {
                        /* x as having ZERO in the centre of the image */
                        int sX = x - mX;

                        /*
                         * Calculate the original pixel usng backwards transform
                         * Simply take the nearest pixel by rounding
                         */
                        int origX = (int)Math.round(cosA*sX + sinA*sY) + mX;
                        int origY = (int)Math.round(-1*sinA*sX + cosA*sY) + mY;

                        /* Check if the original pixel is in the image */
                        if (isValid(origX, origY))
                            argb[offset + x] = origArgb[origY*m_image_width + origX];
                    }
                }
            } else if (m_interpolation == Interpolation.BILINEAIR) {
                /* Bilineair interpolation */
                for (int y = 0; y < m_image_height; y++) {
                    int offset = y * m_image_width;
                    /* y as having ZERO in the centre of the image */
                    int sY = y - mY;
                    for (int x = 0; x < m_image_width; x++) {
                        /* x as having ZERO in the centre of the image */
                        int sX = x - mX;

                        /* Calculate the original pixel usng backwards transform */
                        double origX = cosA*sX + sinA*sY + mX;
                        double origY = -1*sinA*sX + cosA*sY + mY;

                        /* Check if the original pixel is in the image */
                        if (!isValid((int) origX, (int) origY) ||
                            !isValid((int) origX + 1, (int) origY) ||
                            !isValid((int) origX, (int) origY + 1) ||
                            !isValid((int) origX + 1, (int) origY + 1)) {
                            continue;
                        }

                        /* Calculate the array indices of the pixels surrounding the point (origX, origY) */
                        int origTopLeft = ((int) origY) * m_image_width + (int) origX;
                        int origTopRight = ((int) origY) * m_image_width + (int) origX + 1;
                        int origBotLeft = ((int) origY + 1) * m_image_width + (int) origX;
                        int origBotRight = ((int) origY + 1) * m_image_width + (int) origX + 1;

                        /* Calculate the RGB colors of the point above (origX, origY) with its y as an integer */
                        double origTopXRed = (1 - origX % 1) * Color.red(origArgb[origTopLeft]) +
                                (origX % 1) * Color.red(origArgb[origTopRight]);
                        double origTopXGreen = (1 - origX % 1) * Color.green(origArgb[origTopLeft]) +
                                (origX % 1) * Color.green(origArgb[origTopRight]);
                        double origTopXBlue = (1 - origX % 1) * Color.blue(origArgb[origTopLeft]) +
                                (origX % 1) * Color.blue(origArgb[origTopRight]);

                        /* Calculate the RGB colors of the point with below (origX, origY) with its y as an integer */
                        double origBotXRed = (1 - origX % 1) * Color.red(origArgb[origBotLeft]) +
                                (origX % 1) * Color.red(origArgb[origBotRight]);
                        double origBotXGreen = (1 - origX % 1) * Color.green(origArgb[origBotLeft]) +
                                (origX % 1) * Color.green(origArgb[origBotRight]);
                        double origBotXBlue = (1 - origX % 1) * Color.blue(origArgb[origBotLeft]) +
                                (origX % 1) * Color.blue(origArgb[origBotRight]);

                        /* Calculate the RGB colors corresponding to our point (origX, origY) */
                        int red = (int) ((1 - origY % 1) * origTopXRed + (origY % 1) * origBotXRed);
                        int green = (int) ((1 - origY % 1) * origTopXGreen + (origY % 1) * origBotXGreen);
                        int blue = (int) ((1 - origY % 1) * origTopXBlue + (origY % 1) * origBotXBlue);

                        argb[offset + x] = Color.argb(0xFF, red, green, blue);
                    }
                }
            } else {
                argb = origArgb;
            }

            return Bitmap.createBitmap(argb, m_image_width, m_image_height, Bitmap.Config.ARGB_8888);
        }

        private boolean isValid(int x, int y) {
            return (x >= 0 && x < m_image_width && y >= 0 && y < m_image_height);
        }

        @Override
        protected void onPostExecute(Bitmap image) {
            /* Transfer data/results to the canvas... */
            m_canvas_view.setSelectedImage(image);

            /* Invalidate the canvas, forcing it to be redrawn with the new data.
		     * You can do this in other places, evaluate what makes sense to you.
		     */
            m_canvas_view.invalidate();
        }
    }

    /* Accessors */
    public AsyncTask getTask() { return m_task; }

    public void setCanvasView(CanvasView canvas_view) { m_canvas_view = canvas_view; }
    public void setRotationAngle(int rotationAngle) { m_rotation_angle = rotationAngle; }
    public void setInterpolation(Interpolation interpolation) { m_interpolation = interpolation; }
}

